import { useDispatch } from "react-redux";
import {createTodos, loadTodos, loadTodosAllDone, removeTodo, updateTodo as update} from "../api/todo";
import { updateTodoList } from "../app/todoSlice";

const useTodo = () => {
  const dispatch = useDispatch();
  const reloadTodos = async () => {
    const { data } = await loadTodos();
    dispatch(updateTodoList(data));
  };

  const addTodo = async (todoItem) => {
    await createTodos(todoItem);
    await reloadTodos();
  };

  const deleteTodo = async (key)=>{
    await removeTodo(key);
    await reloadTodos();
  }

  const updateTodo = async (id, updateItem) =>{
    await update(id, updateItem);
    await reloadTodos();
  }

  const getDoneList = async () =>{
    const { data } = await loadTodosAllDone();
    return data
  }


  return {
    reloadTodos,
    addTodo,
    deleteTodo,
    updateTodo,
    getDoneList
  }
};

export default useTodo;
