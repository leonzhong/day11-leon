import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import TodoGenerator from "./TodoGenerator.jsx";
import TodoGroup from "./TodoGroup.jsx";
import { loadTodos } from "../api/todo.js";
import { updateTodoList } from "../app/todoSlice.js";
function TodoList() {
  const todos = useSelector((state) => state.todos.todoList);
  const dispatch = useDispatch();

  async function getTodoList() {
    const { data } = await loadTodos();
    dispatch(updateTodoList(data));
  }

  useEffect(() => {
    getTodoList();
  }, []);

  return (
    <div>
      <TodoGenerator />
      <TodoGroup todos={todos} />
    </div>
  );
}

export default TodoList;
