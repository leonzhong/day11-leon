import { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import { createTodos } from "../api/todo";
import useTodo from "../hoos/useTodo";
import { Button, Input } from "antd";
import PKG from '../assets/pkq.svg'


const { TextArea } = Input;
const TodoGenerator = () => {
  const [text, setText] = useState("");
  const { reloadTodos, addTodo } = useTodo();

  const handleAdd = async () => {
    if (text === "") {
      alert("请输入文字");
      return;
    }
    const todoItem = {
      done: false,
      text,
    };
    setText("");
    await addTodo(todoItem)
  };

  return (
    <div className="mt-3 flex justify-center items-center">
      <TextArea
        placeholder="Add something todo."
        autoSize
        
        className="w-2/3 xl:w-1/5 mr-2 "
        value={text}
        onChange={(e) => setText(e.target.value)}
      />
      <Button type={"text"} onClick={handleAdd}>
        <img src={PKG} width={40} height={40}/>
      </Button>
    </div>
  );
};

export default TodoGenerator;
