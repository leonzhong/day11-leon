import { NavLink } from "react-router-dom";
import { Layout } from "antd";
import { Avatar } from "antd";

const { Header } = Layout;

const Navigator = () => {
  const acitveClass = ({ isActive, isPending }) => {
    return {
      textDecoration: isActive ? "underline" : "none",
    };
  };
  return (
    <Header style={{ display: "flex", alignItems: "center" }}>
      <Avatar
        size={64}
        src={
          <img
            src={
              "https://tse1-mm.cn.bing.net/th/id/OIP-C.Nq_75t_GoXkEEqHVgmEh8gHaQC?w=115&h=180&c=7&r=0&o=5&pid=1.7"
            }
          />
        }
      />
      <NavLink style={acitveClass} to="/">
        Home
      </NavLink>
      <NavLink style={acitveClass} to="/about">
        About
      </NavLink>
      <NavLink style={acitveClass} to="/done">
        Done
      </NavLink>
    </Header>
  );
};

export default Navigator;
