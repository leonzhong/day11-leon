import React, {useRef, useState} from "react";
import {useNavigate} from "react-router-dom";
import PKG from '../assets/pokemon.svg'
import {Input, message, Modal} from "antd";


function TodoItem({item, index, onRemove, onUpdate, editAble = true}) {
    const [messageApi, contextHolder] = message.useMessage();
    const [isEdit, setIsEdit] = useState(false);
    const [newVal, setNewVal] = useState(item.text);
    const [visible, setVisible] = useState(false);
    const navigate = useNavigate();
    const ref = useRef(null)

    let timer = null

    const showModal = () => {
        setVisible(true);
    };

    const handleCancel = () => {
        setVisible(false);
        setIsEdit(false)
    };

    const handleOk = async () => {
        // handle input
        await handleSave()
        setIsEdit(false)
        setVisible(false);
    };

    const handleInputChange = (e) => {
        console.log(e.target.value)
        setNewVal(e.target.value);
    };
    const onChange = (newVal) => {
        setNewVal(newVal);
    };
    const handleSave = async (event) => {
        if(event){
            event.stopPropagation()
        }
        if (newVal === "") {
            await messageApi.error("todo is not null")
            return;
        }
        onUpdate(index, {...item, text: newVal});
        setIsEdit(false);
    };
    const handleEdit = () => {
        setNewVal(item.text);
        showModal()
        setIsEdit(true)
    };

    const handleToggle = (event) => {
        clearTimeout(timer)
        if (editAble && event.detail === 1) {
            timer = setTimeout(() => {
                console.log('click')
                onUpdate(index, {...item, done: !item.done});
            }, 200)
        } else if (editAble && event.detail === 2) {
            handleEdit()
        } else {
            navigate(`/todo/${item.id}`);

        }
    };

    const handleKeyUp = ({keyCode}) => {
        if (keyCode === 13) {
            handleSave();
        }
    };

    const handleRemove = (event) => {
        event.stopPropagation()
        return onRemove(item.id);
    };

    return (
        <>
            <Modal
                title="Update Todo.."
                open={visible}
                onCancel={handleCancel}
                onOk={handleOk}
            >
                <Input
                    placeholder="Input..."
                    value={newVal}
                    onChange={handleInputChange}
                />
            </Modal>
            {contextHolder}
            <li ref={ref} onClick={handleToggle}
                className=" flex items-center justify-between p-2.5 border-b border-gray-100 list-none">
                {isEdit ? (
                    <>
                        <Input
                            bordered={false}
                            style={{width: '40%'}}
                            value={newVal}
                            autoFocus
                            onChange={(event) => onChange(event.target.value)}
                            onKeyUp={handleKeyUp}
                        />

                    </>

                ) : (
                    <span className={item.done ? "line-through" : null}>{item.text}</span>
                )}
                {editAble && (
                    <div className="flex items-center justify-center">
                        {isEdit === true ?
                            <button className="text-red-500 ml-2" onClick={handleSave}>
                                <img src={PKG} className="w-9 h-9" alt="save"/>
                            </button> :
                            <button className="text-red-500 ml-2" onClick={handleRemove}>
                                <img src={PKG} className="w-9 h-9" alt="delete"/>
                            </button>}
                    </div>
                )}
            </li>
        </>

    );
}

export default TodoItem;
