import React from "react";
import TodoItem from "./TodoItem.jsx";
import useTodo from "../hoos/useTodo.js";

function TodoGroup({todos, editAble = true}) {
    const {reloadTodos, deleteTodo, updateTodo} = useTodo();
    const handleRemove = async (key) => {
        await deleteTodo(key)
    };
    const handleUpdate = async (index, updateItem) => {
        await updateTodo(updateItem.id, updateItem)
    };
    return (
        <main className="p-4 max-w-xl mx-auto">
            <ul className="border-2 border-b-cyan-50  rounded-2xl p-2 shadow-2xl">
                {todos.map((todo, index) => (
                    <TodoItem
                        key={todo.id}
                        item={todo}
                        index={index}
                        onRemove={handleRemove}
                        onUpdate={handleUpdate}
                        editAble={editAble}
                    />
                ))}
            </ul>
        </main>
    );
}

export default TodoGroup;
