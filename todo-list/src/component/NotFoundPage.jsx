import Img from "../assets/MicrosoftTeams-image.png";
import { useNavigate } from "react-router-dom";
import { Button } from "antd";
const NotFoundPage = () => {
  const navigate = useNavigate();
  const handleBack = () => {
    navigate("/");
  };
  return (
    <div className="flex justify-center items-center flex-col h-screen">
      <div className="text-center font-bold text-4xl">404 Not Found!</div>
      <div className="text-center mt-10">
        <Button type="primary" onClick={handleBack}>
          Back to home!
        </Button>
      </div>
    </div>
  );
};

export default NotFoundPage;
