import { Outlet } from "react-router-dom";
import Navigator from "./Navigator";
import { Layout as ALayout } from "antd";
const { Content } = ALayout;
const Layout = () => {
  return (
    <>
      <Navigator />
      <Content>
        <Outlet />
      </Content>
      {/* <main className="p-4 max-w-2xl w-96 mx-auto">
      </main> */}
    </>
  );
};

export default Layout;
