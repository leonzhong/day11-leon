import { useSelector } from "react-redux";
import TodoGroup from "../../component/TodoGroup";
import { useEffect, useState } from "react";
import { loadTodosAllDone } from "../../api/todo";
import useTodo from "../../hoos/useTodo";
const DoneList = () => {
  const [todos, setTodos] = useState([])
  const {getDoneList} = useTodo();

  async function initDoneList() {
    const data = await getDoneList()
    setTodos(data)
  }

  useEffect(() => {
    initDoneList()
  },[]);

  const doneTodos = todos.filter((todo) => todo.done);

  return (
    <div>
      <h3 className="text-center font-bold">DONE LIST</h3>

      <TodoGroup todos={doneTodos} editAble={false} />
    </div>
  );
};

export default DoneList;
