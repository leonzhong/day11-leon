import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams, useSearchParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { getTodoById } from "../../api/todo";

const TodoDetail = () => {
  const navigate = useNavigate();
  const params = useParams();
  const { id } = params;
  const [todo, setTodo] = useState(null);

  const loadById = async () => {
    const { data } = await getTodoById(id);
    setTodo(data);
    if (!data) {
      navigate("/404");
    }
  };

  useEffect(() => {
    loadById();
  }, []);
  return (
    <article className={"flex justify-center items-center font-bold h-80"}>
      <h2 className="text-3xl">{todo && todo.text}</h2>
    </article>
  );
};

export default TodoDetail;
