import { createBrowserRouter } from "react-router-dom";
import Layout from "./component/Layout";
import TodoList from "./component/TodoList";
import About from "./page/About";
import DoneList from "./page/todo/DoneList";
import TodoDetail from "./page/todo/TodoDetail";
import NotFoundPage from "./component/NotFoundPage";
export const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "/",
        element: <TodoList />,
      },
      {
        path: "about",
        element: <About />,
      },
      {
        path: "done",
        element: <DoneList />,
      },
      {
        path: "todo/:id",
        element: <TodoDetail />,
      },
    ],
  },
  {
    path: "*",
    element: <NotFoundPage />,
  },
]);
