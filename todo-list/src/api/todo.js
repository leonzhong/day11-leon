import request from "./request";

export const loadTodos = () => {
  return request.get("/todos");
};

export const createTodos = (data) => {
  return request.post("/todos", data);
};

export const removeTodo = (id) => {
  return request.delete(`/todos/${id}`);
};
export const updateTodo = (id, data) => {
  return request.put(`/todos/${id}`, data);
};

export const loadTodosAllDone = () => {
  return request.get("/todos/done");
};

export const getTodoById = (id) => {
  return request.get(`/todos/${id}`)
};
