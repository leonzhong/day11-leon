import { createSlice } from "@reduxjs/toolkit";
import { createTodos, removeTodo } from "../api/todo";
import { updateTodo } from "../api/todo";
export const todoSlice = createSlice({
  name: "todo",
  initialState: {
    todoList: [],
  },
  reducers: {
    updateTodoList: (state, action) => {
      state.todoList = [...action.payload];
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  updateTodoList,
} = todoSlice.actions;

export default todoSlice.reducer;
