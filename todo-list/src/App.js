import "./App.css";
import { RouterProvider } from "react-router-dom";
import { router } from "./router";
import Navigator from "./component/Navigator";
export default function App() {
  return (
    <>
      <RouterProvider router={router}></RouterProvider>
      {/* <TodoList/> */}
    </>
  );
}
