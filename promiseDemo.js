const fs = require('fs')
const readFile = (filename) => {
    return new Promise((resolve, reject) => {
        fs.readFile(filename, (err, data) => {
            if (err) {
                reject(err)
            } else resolve(data);
        });
    });
}


const handleSuccess = (data) => {
    console.log('读取文件成功:', data.toString());
}


const handleError = (err) => {
    console.error('读取文件失败:', err);
}


readFile('file1.txt')
    .then(data => {
        handleSuccess(data)
        return readFile("file2.txt");
    })
    .then(data => {
        handleSuccess(data);
    })
    .catch(err => {
        handleError(err);
    });

async function asyncFn() {
    try {
        const data = await readFile('file1.txt');
        handleSuccess(data)
    } catch (e) {
        handleError(e)
    }
}

asyncFn()
