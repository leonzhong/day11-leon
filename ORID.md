Objective:

- Yesterday we learned about React Router and asynchronous request fetching, and in the afternoon we connected mock data to rewrite the logic from Redux.

Reflective:

- Having been familiar with the React ecosystem, I felt that the pace of learning last week was a bit fast.

Interpretive:

- Although I was able to keep up with the teacher's pace during class, there were a lot of knowledge points, and I had to spend a lot of time reviewing what we learned that day for homework.

Decisional:

- I decided to continue learning React-related knowledge and delve deeper into React Router.